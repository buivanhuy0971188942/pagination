import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  constructor(private http: HttpClient) {}
  redirectUrl: string | null = null;

  getUsers(page = 1, results = 10) {
    return this.http.get(
      'https://randomuser.me/api/' + `?page=${page}&results=${results}`
    );
  }
}
