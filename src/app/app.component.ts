import { AppService } from './app.service';
import { Component } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  title = 'pagination-test';
  users: any = {};
  usersItems = [];
  displayedColumns: string[] = ['name', 'login', 'picture'];

  constructor(private router: Router, private userService: AppService) {}

  getUsers(page?: number | undefined, results?: number | undefined) {
    this.userService.getUsers(page, results).subscribe({
      next: (usersResp: any) => {
        this.users = usersResp;
        this.usersItems = usersResp['results'].sort((a: any, b: any) => {
          const usernameA = a.login.username; // ignore upper and lowercase
          // .toLowerCase();
          const usernameB = b.login.username; // ignore upper and lowercase
          // .toLowerCase();
          if (usernameA < usernameB) {
            return -1;
          }
          if (usernameA > usernameB) {
            return 1;
          }

          // usernames must be equal
          return 0;
        });
        console.log(this.users, this.usersItems);
      },
    });
    console.log('pagination');
  }

  ngOnInit(): void {
    this.getUsers();
  }

  handlePageEvent(event: PageEvent) {
    console.log(event);
    this.getUsers(event.pageIndex + 1, event.pageSize);
  }
}
