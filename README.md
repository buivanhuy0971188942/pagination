# PaginationTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.0.

## Clone source code

Run `git clone https://gitlab.com/buivanhuy0971188942/pagination` for clone source code. Then, you you need to run the command `npm i` to install the project's module packages

## Development server

Run `ng serve` or `npm start` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

